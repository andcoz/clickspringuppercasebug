package pages;

import javax.annotation.Resource;

import org.apache.click.Page;
import org.apache.click.util.Bindable;
import org.springframework.stereotype.Component;

import beans.SampleInterface;

@Component
public class BCdefPage extends Page {
    private static final long serialVersionUID = 6365945348923571234L;

    @Resource(name = "sampleBean")
    protected SampleInterface sample;

    @Bindable
    protected Integer sampleId = 0;

    public BCdefPage() {
        super();
    }

    @Override
    public void onInit() {
        super.onInit();

        sampleId = sample.getNumber();
    }

    @Override
    public void onRender() {
        super.onRender();
    }
}