Apache Click 2.3.0 SpringClickServlet Bug Demonstrator
======================

I am using `SpringClickServlet` to integrate Click with Spring.

Spring injection in `Page` classes works only if the class name starts with **exactly one** upper case letter. 

E.g. I have a class called `ABTestPage` (that extends `org.apache.click.Page` and that is a `@Component`). When I try to use it, any `@Resource` will be `null`. 
If I rename the class to `AbTestPage`, any `@Resource` will be correctly injected.

The following simple project demonstrates the problem.

The *eclipse* directory contains the STS project. The *dist* directory contains a *war* ready to deploy on Apache Tomcat.

This bug is reported as [CLK-789](https://issues.apache.org/jira/browse/CLK-789) on Apache Click JIRA.

Test Environment
---------------------
* Apache Tomcat 6.0.32 or 7.0.16
* JRE 1.6.0_29 on Linux
* Spring Framework 3.0.5
* Apache Click 2.3.0